package config

import (
	"fmt"
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

type AttributePermissions struct {
	Get []string `yaml:"get"` // List of attribute names. If it contains a '*' element, then all attributes in the rubriek/proces are allowed
	Set []string `yaml:"set"`
}

type Role struct {
	parentConfig *Config // The config the role belongs to. Used to calculate '*' permissions

	Name        string `yaml:"name"`
	Label       string `yaml:"label"`
	Permissions struct {
		BVVWriteAccess bool `yaml:"bvvWriteAccess"`

		Rubrieken map[string]AttributePermissions `yaml:"rubrieken"` // Map keys are the rubriek names
		Processen map[string]AttributePermissions `yaml:"processen"` // Map keys are the proces names
	}
}

type Attribute struct {
	Name     string      `yaml:"name"` // Note: in case of a rubriek, the name must be unique across all rubrieken
	Label    string      `yaml:"label"`
	Type     string      `yaml:"type"`
	Multiple bool        `yaml:"multiple"` // For types 'select' and 'file'
	Default  interface{} `yaml:"default"`

	// For number types
	Min  interface{} `yaml:"min"`
	Max  interface{} `yaml:"max"`
	Step interface{} `yaml:"step"`

	// For select, radios, and checkboxes types
	Options []struct {
		Name  string `yaml:"name"`
		Label string `yaml:"label"`
	} `yaml:"options"`
}

type Rubriek struct {
	Name  string `yaml:"name"`
	Label string `yaml:"label"`

	Attributes []Attribute `yaml:"attributes"`
}

type Proces struct {
	Name     string `yaml:"name"`
	Label    string `yaml:"label"`
	Statuses []struct {
		Name  string `yaml:"name"`
		Label string `yaml:"label"`
	} `yaml:"statuses"`

	Attributes []Attribute `yaml:"attributes"`
}

type GlobalConfig struct {
	Organizations map[string]string `yaml:"organizations"` // Map from organization names to labels (pretty names)
	Rubrieken     []Rubriek         `yaml:"rubrieken"`
	Processen     []Proces          `yaml:"processen"`
}

type OrganizationConfig struct {
	Organization struct {
		Name                string   `yaml:"name"`
		Theme               string   `yaml:"theme"`
		BVVWriteAccess      bool     `yaml:"bvvWriteAccess"`
		ProcesNotifications []string `yaml:"procesNotifications"` // Contains the names/IDs of the processes that the organization is notified about when enabled on vreemdeling-level
	} `yaml:"organization"`

	Roles []Role

	ListenAddress string `yaml:"listenAddress"`
}

type FscConfig map[string]struct {
	Endpoint     string `yaml:"endpoint"`
	FscGrantHash string `yaml:"FscGrantHash"`
}

type Config struct {
	GlobalConfig
	OrganizationConfig
	FscConfig
}

type permissionType string

const (
	permissionsGet permissionType = "get"
	permissionsSet permissionType = "set"
)

// New composes a config with values from the specified paths
func New(globalConfigPath string, organizationConfigPath string, fscConfigPath string) (cfg Config, err error) {
	var yamlFile []byte
	if yamlFile, err = os.ReadFile(globalConfigPath); err != nil {
		err = fmt.Errorf("error reading global config file: %w", err)
		return
	}

	if err = yaml.Unmarshal(yamlFile, &cfg.GlobalConfig); err != nil {
		err = fmt.Errorf("error unmarshalling global config: %w", err)
		return
	}

	if yamlFile, err = os.ReadFile(organizationConfigPath); err != nil {
		err = fmt.Errorf("error reading organization config file: %w", err)
		return
	}

	if err = yaml.Unmarshal(yamlFile, &cfg.OrganizationConfig); err != nil {
		err = fmt.Errorf("error unmarshalling organization config: %w", err)
		return
	}

	if yamlFile, err = os.ReadFile(fscConfigPath); err != nil {
		err = fmt.Errorf("error reading FSC config file: %w", err)
		return
	}

	if err = yaml.Unmarshal(yamlFile, &cfg.FscConfig); err != nil {
		err = fmt.Errorf("error unmarshalling FSC config: %w", err)
		return
	}

	if cfg.ListenAddress == "" {
		cfg.ListenAddress = "0.0.0.0:80" // Default port if not set in config
	}

	// For each role, set the parentConfig value
	for i := range cfg.Roles {
		cfg.Roles[i].parentConfig = &cfg // Note: set using [i], in order to update the role inside the loop
	}

	return
}

func (r Role) ReadableRubriekAttributes() []string {
	return r.accessibleRubriekAttributes(permissionsGet)
}

func (r Role) WritableRubriekAttributes() []string {
	return r.accessibleRubriekAttributes(permissionsSet)
}

func (r *Role) accessibleRubriekAttributes(pt permissionType) (attrs []string) { // Note: not using this method directly, since in Go templates, only niladic methods (i.e. without arguments) are accessible. Also below
	for rubriekName, rubriekPermissions := range r.Permissions.Rubrieken {
		var rubriekAttrs []string
		switch pt {
		case permissionsGet:
			rubriekAttrs = rubriekPermissions.Get

		case permissionsSet:
			rubriekAttrs = rubriekPermissions.Set

		default:
			log.Printf("unexpected permission type: %s", pt)
			return nil
		}

		// In case one of the permissions is '*', use all of the rubriek's attributes from the config
		for _, at := range rubriekAttrs {
			if at == "*" {
				if r.parentConfig == nil {
					log.Printf("error getting parent config of role %s", r.Name)
					return nil
				}

				for _, configRubriek := range r.parentConfig.Rubrieken {
					if configRubriek.Name == rubriekName {
						rubriekAttrs = make([]string, len(configRubriek.Attributes))
						for i, crAttribute := range configRubriek.Attributes {
							rubriekAttrs[i] = crAttribute.Name
						}

						break
					}
				}

				break
			}
		}

		attrs = append(attrs, rubriekAttrs...)
	}

	return
}

func (r Role) ReadableProcesAttributes() map[string][]string {
	return r.accessibleProcesAttributes(permissionsGet)
}

func (r Role) WritableProcesAttributes() map[string][]string {
	return r.accessibleProcesAttributes(permissionsSet)
}

func (r *Role) accessibleProcesAttributes(pt permissionType) (attrs map[string][]string) {
	attrs = make(map[string][]string)

	for procesName, procesPermissions := range r.Permissions.Processen {
		var procesAttrs []string
		switch pt {
		case "get":
			procesAttrs = procesPermissions.Get

		case "set":
			procesAttrs = procesPermissions.Set

		default:
			log.Printf("unexpected permission type: %s", pt)
			return nil
		}

		// In case one of the permissions is '*', use all of the rubriek's attributes from the config
		for _, at := range procesAttrs {
			if at == "*" {
				if r.parentConfig == nil {
					log.Printf("error getting parent config of role %s", r.Name)
					return nil
				}

				for _, configProces := range r.parentConfig.Processen {
					if configProces.Name == procesName {
						procesAttrs = make([]string, len(configProces.Attributes))
						for i, cpAttribute := range configProces.Attributes {
							procesAttrs[i] = cpAttribute.Name
						}

						break
					}
				}

				break
			}
		}

		attrs[procesName] = append(attrs[procesName], procesAttrs...)
	}

	return
}
